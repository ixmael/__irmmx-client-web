import Vue from 'vue';
import VueI18n from 'vue-i18n';

import languages from './locales';

Vue.use(VueI18n);

const messages = Object.assign(languages);

export default new VueI18n({
  locale: process.env.VUE_APP_I18N_LOCALE || 'es',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'es',
  messages,
});
