/* global Then */

'use strict';

Then(/^the tag (.+) exists$/, (tag) => {
  cy.get(tag).should('exist');
});

Then(/^the tag (.+) contains message \"([^\"]+)\"$/, (tag, message) => { //eslint-disable-line
  cy.get(tag).contains(message);
});
