/* global Given */

'use strict';

Given(/^visit page (.+)$/, (page) => {
  let url = '';
  switch (page) {
    default:
      url = '/';
  }

  cy.visit(url);
});
