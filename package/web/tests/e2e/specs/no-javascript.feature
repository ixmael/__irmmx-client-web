Feature: Disabled javascript

  The page requires to display a message error when the browser hasn't the
  javascript engine enable.

  Scenario: User visit the main page with the javascript disabled
    Given visit page home
    Then the tag noscript exists
      And the tag noscript contains message "This web requires the JavaScript enabled."
